import { Category } from 'src/categories/entities/category.entity';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Category)
    private categoriesRepository: Repository<Category>,
  ) {}

  async create(createProductDto: CreateProductDto) {
    const category = await this.categoriesRepository.findOne({
      where: { id: createProductDto.categoryId },
    });
    const product = new Product();
    product.name = createProductDto.name;
    product.image = createProductDto.image;
    product.price = createProductDto.price;
    product.category = category;
    return this.productsRepository.save(product);
  }

  findAll(option) {
    return this.productsRepository.find(option);
  }

  findฺByCategory(id: number) {
    return this.productsRepository.find({ where: { categoryId: id } });
  }

  findOne(id: number) {
    return this.productsRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    try {
      const updatedProduct = await this.productsRepository.save({
        id,
        ...updateProductDto,
      });
      return updatedProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const product = await this.productsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedProduct = await this.productsRepository.remove(product);
      return deletedProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
